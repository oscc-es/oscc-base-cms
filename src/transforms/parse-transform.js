const jsdom = require('@tbranyen/jsdom');
const {JSDOM} = jsdom;
const imageShortcode = require('../shortcodes/image-shortcode.js');

module.exports = function (value, outputPath) {
  if (outputPath && outputPath.endsWith('.html')) {
    const DOM = new JSDOM(value, {
      resources: 'usable'
    });

    const document = DOM.window.document;
    const articleImages = [...document.querySelectorAll('img:not([decoding])')];
    const externalLinks = [...document.querySelectorAll('main a[href^="https://"]')];

    if (externalLinks.length) {
      externalLinks.forEach(link => link.setAttribute('rel', 'external'));
    }

    if (articleImages.length) {
      articleImages.forEach(img => {
        const imgAttr = {
          src: img.getAttribute('src'),
          alt: img.getAttribute('alt')
        };

        const title = img.getAttribute('title');

        if (title) {
          imgAttr.title = title;
          imgAttr.figure = true;
        }

        const selector = title ? 'figure' : 'picture';
        const auxDom = new jsdom.JSDOM(`<!DOCTYPE html>${imageShortcode(imgAttr)}`);
        let imgWrapped = auxDom.window.document.querySelector(selector);

        img.replaceWith(imgWrapped);
      });
    }

    return '<!DOCTYPE html>\r\n' + document.documentElement.outerHTML;
  }
  return value;
};
