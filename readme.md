# Project base for Eleventy Sites with blog and CMS

Based on Andy Bell's [11ty-base](https://github.com/hankchizljaw/11ty-base) on top of my [clean-commits template](https://gitlab.com/oscc-es/clean-commits).

## Additional features

- [Static CMS](https://github.com/StaticJsCMS/static-cms) and Netlify Identity
- Minimal modern favicon following the [Definitive edition of "How to Favicon in 2021"](https://dev.to/masakudamatsu/favicon-nightmare-how-to-maintain-sanity-3al7)
- sitemap.xml
- robots.txt
- Security HTTP headers
- 404 page
- Autoprefix and minify production css (with [Lightning CSS](https://lightningcss.dev/))
- [Automatically generate open graph images in Bernard Nijenhuis's way](https://bnijenhuis.nl/notes/2021-05-10-automatically-generate-open-graph-images-in-eleventy/)
- Minify production js
- Optimise size and image formats
- Image shorcode to wrap img with multi-source/sizes picture tags
- Service worker for PWA (Andy Bell's [Hylia](https://github.com/hankchizljaw/hylia) has been used as base)
- Offline page
- Fluid font-size (made with [Utopia](https://utopia.fyi/))

## Setting up

- Change `display_url: https://example.com` and `site_url: https://example.com` at `src/admin/config.yml`
- Configure Netlify's [Git Gateway ](https://docs.netlify.com/visitor-access/git-gateway/)

## Getting started

- Run `npm install`
- Run `npm start` to run locally
- Run `npm run production` to do a prod build
- Run `npm run release` to generate/update the changelog file and increment the corresponding npm version on package.json

To test the backend locally:

- Uncomment line #6 (`# local_backend: true`) from `src/admin/config.yml`
- Run `npx @staticcms/proxy-server`
