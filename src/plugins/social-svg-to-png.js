// https://bnijenhuis.nl/notes/2021-05-10-automatically-generate-open-graph-images-in-eleventy/

const fs = require('fs');
const Image = require('@11ty/eleventy-img');

module.exports = function (
  eleventyConfig,
  socialPreviewImagesDir = 'dist/images/social-images/',
  env = 'production'
) {
  if (process.env.NODE_ENV !== env) {
    return;
  }

  eleventyConfig.on('eleventy.after', () => {
    if (!fs.existsSync(socialPreviewImagesDir)) {
      throw new Error(
        `SVG social-images directory does not exist: ${socialPreviewImagesDir}`
      );
    }

    fs.readdir(socialPreviewImagesDir, (err, files) => {
      if (files.length <= 0) {
        return;
      }

      files.forEach(filename => {
        if (!filename.endsWith('.svg')) {
          return;
        }

        const imageUrl = `${socialPreviewImagesDir}${filename}`;
        Image(imageUrl, {
          formats: ['png'],
          outputDir: `./${socialPreviewImagesDir}`,
          filenameFormat: (id, src, width, format, options) => {
            const outputFilename = filename.substring(0, filename.length - 4);

            return `${outputFilename}.${format}`;
          }
        });
      });
    });
  });
};
